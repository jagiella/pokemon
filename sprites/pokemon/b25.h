
//{{BLOCK(b25)

//======================================================================
//
//	b25, 64x64@4, 
//	+ palette 16 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2020-03-30, 00:52:46
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_B25_H
#define GRIT_B25_H

#define b25TilesLen 2048
extern const unsigned int b25Tiles[512];

#define b25PalLen 32
extern const unsigned short b25Pal[16];

#endif // GRIT_B25_H

//}}BLOCK(b25)
