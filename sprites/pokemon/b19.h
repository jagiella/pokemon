
//{{BLOCK(b19)

//======================================================================
//
//	b19, 64x64@4, 
//	+ palette 16 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 32 + 2048 = 2080
//
//	Time-stamp: 2020-03-30, 00:52:46
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_B19_H
#define GRIT_B19_H

#define b19TilesLen 2048
extern const unsigned int b19Tiles[512];

#define b19PalLen 32
extern const unsigned short b19Pal[16];

#endif // GRIT_B19_H

//}}BLOCK(b19)
