
//{{BLOCK(ashSheet)

//======================================================================
//
//	ashSheet, 512x32@4, 
//	+ palette 16 entries, not compressed
//	+ 256 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 8192 = 8224
//
//	Time-stamp: 2020-03-28, 00:40:38
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_ASHSHEET_H
#define GRIT_ASHSHEET_H

#define ashSheetTilesLen 8192
extern const unsigned int ashSheetTiles[2048];

#define ashSheetPalLen 32
extern const unsigned short ashSheetPal[16];

#endif // GRIT_ASHSHEET_H

//}}BLOCK(ashSheet)
