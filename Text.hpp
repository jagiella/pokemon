/*
 * Text.hpp
 *
 *  Created on: 21 Apr 2020
 *      Author: jagiella
 */

#ifndef TEXT_HPP_
#define TEXT_HPP_

#include <string.h>

class Text: public Object{
	int tid,pb;
	int dx = 5;
public:
	//OBJ_ATTR *objs;
	//int length;
	Text(): Object(){

		//tid = SpriteEngine::instance()->addTiles(  fontTiles, fontTilesLen);
		//pb = SpriteEngine::instance()->addPalette( fontPal,   fontPalLen);
	}
	Text( int tileID, int textPaletteBank): tid(tileID), pb(textPaletteBank){
	}

	/*void usePalette( int paletteBankIndex){
		pb = paletteBankIndex;
	}*/

	void putxy(const char* str, int x, int y){
		//length = strlen(str);
		//objs = (OBJ_ATTR*) malloc( length*sizeof(OBJ_ATTR));
		allocate( strlen(str));
		for( int i=0; i<count(); i++){
			obj_set_attr( &sprites()[i],
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_8x8,              // 64x64p,
					ATTR2_PALBANK(pb) | (tid+str[i]));
			obj_set_pos( &sprites()[i], x+i*dx, y);

		}

		setRepaint(true);
	}
	void moveto(int x, int y){
		for( int i=0; i<count(); i++){
			obj_set_pos( &sprites()[i], x+i*dx, y);
		}
		setRepaint(true);
	}

};



#endif /* TEXT_HPP_ */
