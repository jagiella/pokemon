/*
 * pokemon.cpp
 *
 *  Created on: 23.03.2020
 *      Author: jagiella
 */

#include "SpriteEngine.h"
#include "Object.hpp"
#include "Text.hpp"

#include "ashSheet.h"
#include "mistySheet.h"
#include "pikachuSheet.h"
#include "starmieSheet.h"

#include "tilesets/font.h"

#include "sprites/pokemon/f19.h"
#include "sprites/pokemon/f25.h"
#include "sprites/pokemon/b19.h"
#include "sprites/pokemon/b25.h"

#include "Tilemap.h"
#include "tilemaps/litaCity.h"
#include "tilemaps/battle/dialog1.h"
#include "tilemaps/battle/ground1.h"

#include <stdlib.h>

#include <list>

extern "C" {
#include "tonc.h"
#include "tonc_video.h"
#include "tonc_input.h"
#include "tonc_bios.h"
#include "tonc_nocash.h"
}

//#define SCR_HEIGHT 160
//#define SCR_WIDTH  240

#define SIGN(x) (x<0?-1:(x==0?0:+1))
#define ABS(x)  (x<0?-x:x)
#define MAX(a,b)(a>b?a:b)



enum PokemonType {
	Rattfratz,
	Pikachu,
	Starmie,
};

const char *PokemonNames[] = {
	"Rattfratz",
	"Pikachu",
	"Starmie"
};

enum PowerType {
	Nichts=0,
	Donnerschlag=1,
	Heuler=2
};

const char *PowerNames[] = {
	"...",
	"Donnerschlag",
	"Heuler"
};




//int objCount = 0;









class Pokemon {
private:
public:
	int level;
	int type;
	int health;
	int maxHealth;

public:
	int powers[4];
	Pokemon( int type, int level): level(level), type(type){
		powers[0] = PowerType::Donnerschlag;
		powers[1] = PowerType::Heuler;
		powers[2] = PowerType::Nichts;
		powers[3] = PowerType::Nichts;

		maxHealth = 100;
		health    = 100;
	}
};

class Companion: public Object{
	unsigned int tid;
	unsigned int pb;
	//OBJ_ATTR *obj;
	int x=0,y=0;
	int dirh = -1;
	int dirv = -1;
	int animIndex = 0;

	int powers[4];

public:
	enum Characters {Pikachu, Starmie};

	Companion( int characterID){
		switch( characterID){
		case Starmie:
			tid = SpriteEngine::instance()->addTiles( starmieSheetTiles, starmieSheetTilesLen);
			pb = SpriteEngine::instance()->addPalette( starmieSheetPal, starmieSheetPalLen);
			break;
		case Pikachu:
		default:
			tid = SpriteEngine::instance()->addTiles( pikachuSheetTiles, pikachuSheetTilesLen);
			pb = SpriteEngine::instance()->addPalette( pikachuSheetPal, pikachuSheetPalLen);
		}
		//obj = &obj_buffer[objCount++];
		allocate( 1);
		OBJ_ATTR *obj = sprites();

		obj_set_attr( obj,
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_32x32,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(pb) | tid);
		//x=0;
		//y=0;
	}

	void update( int x, int y){
		OBJ_ATTR *obj = sprites();

		int dx = -(this->x - x);
		int dy = -(this->y - y);

		if( ABS(dx) < 16)
			dx = 0;
		else{
			dx = SIGN(dx);
			animIndex++;
			dirh = dx;
		}
		if( ABS(dy) < 16)
			dy = 0;
		else{
			dy = SIGN(dy);
			animIndex++;
			dirv = dy;
		}

		//if( ABS(dx) > ABS(dy))
			this->x += dx;
		//else
			this->y += dy;

		if( ABS(dx) > ABS(dy)){
			switch( dirh){
			case -1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(0+animIndex/5 % 4));
				break;
			case +1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(4+((animIndex/5) % 4)));
				break;
			}
		}else if( ABS(dy) > 0){
			switch( dirv){
			case +1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(8+animIndex/5 % 4));
				break;
			case -1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(12+((animIndex/5) % 4)));
				break;
			}
		}
	}

	void updateScreen( int offsetx, int offsety){
		OBJ_ATTR *obj = sprites();
		obj_set_pos( obj, this->x-offsetx, this->y-offsety);
	}
};


class Player: public Object{
	unsigned int tid;
	unsigned int pb;
	//OBJ_ATTR *obj;
	int dirh = -1;
	int dirv = -1;
	int animIndex = 0;
public:
	int x=0,y=0;
	enum {Ash, Misty};

	Player( int characterID = Ash){
		switch( characterID){
		case Ash:
			tid = SpriteEngine::instance()->addTiles( ashSheetTiles, ashSheetTilesLen);
			pb = SpriteEngine::instance()->addPalette( ashSheetPal, ashSheetPalLen);
			break;
		case Misty:
			tid = SpriteEngine::instance()->addTiles( mistySheetTiles, mistySheetTilesLen);
			pb = SpriteEngine::instance()->addPalette( mistySheetPal, mistySheetPalLen);
			break;
		}
		allocate(1);
		//obj = &obj_buffer[objCount++];


		obj_set_attr( sprites(),
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_32x32,              // 64x64p,
					//ATTR1_SIZE_16x32,              // 64x64p,
					ATTR2_PALBANK(pb) | tid);
		//x=0;
		//y=0;
	}

	void update(){
		animIndex += MAX( ABS( key_tri_horz()), ABS( key_tri_vert()));
		if( key_tri_horz() != 0)
			dirh = key_tri_horz();
		if( key_tri_vert() != 0)
			dirv = key_tri_vert();

		x += key_tri_horz();
		y += key_tri_vert();


		OBJ_ATTR *obj = sprites();

		if( key_tri_horz() != 0){
			switch( dirh){
			case -1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(0+animIndex/5 % 4));
				break;
			case +1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(4+((animIndex/5) % 4)));
				break;
			}
		}else if( key_tri_vert() != 0){
			switch( dirv){
			case +1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(8+animIndex/5 % 4));
				break;
			case -1:
				obj->attr2 = ATTR2_PALBANK(pb) | (tid + 4*4*(12+((animIndex/5) % 4)));
				break;
			}
		}
	}

	void updateScreen( int offsetx, int offsety){
		obj_set_pos( sprites(), this->x-offsetx, this->y-offsety);
	}
};

class Text;

class Battle: public Object{
	unsigned int tid1, pb1;
	unsigned int tid2, pb2;

	Pokemon *pokemon1;
	Text *pokemonName;
	//OBJ_ATTR *obj1, *obj2;
public:
	Battle(Player *player, int pokemonID){

		pokemon1 = new Pokemon( pokemonID, 3);
		tid1 = SpriteEngine::instance()->addTiles( b25Tiles, b25TilesLen);
		pb1  = SpriteEngine::instance()->addPalette( b25Pal, b25PalLen);

		tid2 = SpriteEngine::instance()->addTiles( f19Tiles, f19TilesLen);
		pb2  = SpriteEngine::instance()->addPalette( f19Pal, f19PalLen);

		allocate(2);
		OBJ_ATTR *obj1 = &sprites()[0], *obj2 = &sprites()[1];


		obj_set_attr( obj1,
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_64x64,              // 64x64p,
					ATTR2_PALBANK(pb1) | tid1);
		obj_set_attr( obj2,
					ATTR0_SQUARE | ATTR0_4BPP,              // Square, regular sprite
					ATTR1_SIZE_64x64,              // 64x64p,
					ATTR2_PALBANK(pb2) | tid2);

		obj_set_pos( obj1, 32, SCR_H-64-40);
		obj_set_pos( obj2, SCR_W-64-32, 16);

		setRepaint(true);

		u32 tid = SpriteEngine::instance()->addTiles( fontTiles, fontTilesLen);
		u32 pb  = SpriteEngine::instance()->addPalette( fontPal, fontPalLen);
		pokemonName = new Text(tid, pb);
		pokemonName->putxy( PokemonNames[ pokemonID], 15,20);

		//text2->putxy( PowerNames[ pokemon->powers[1]], 15,128);
	}

	void setup(){
		// BATTLE BACKGROUND

		memcpy32(pal_bg_bank[0], ground1Pal, ground1PalLen/4);
		memcpy32(tile_mem[0], ground1Tiles, ground1TilesLen/4);
		memcpy16( se_mem[4], ground1Map, ground1MapLen);
		REG_BG0CNT = BG_CBB(0) | BG_SBB(4) | BG_4BPP | BG_REG_32x32 | BG_PRIO(0);
		REG_BG0HOFS = 0;
		REG_BG0VOFS = 0;

		// MENU

		memcpy32(pal_bg_bank[1], dialog1Pal, dialog1PalLen/4);
		memcpy32(tile_mem[1], dialog1Tiles, dialog1TilesLen/4);
		memcpy16( se_mem[12], dialog1Map, dialog1MapLen);
		REG_BG1CNT = BG_CBB(1) | BG_SBB(12) | BG_4BPP | BG_REG_32x32 | BG_PRIO(0);
		REG_BG1HOFS = 0;
		REG_BG1VOFS = -112;

		pokemonName->show();
	}

	void update(){
		if( key_hit( KEY_B)){
			//cursorPos = (cursorPos + 1) % 4;
			char text[20];

			pokemon1->health -= 5;
			sprintf(text, "%s: %d / %d", PokemonNames[ pokemon1->type], pokemon1->health, pokemon1->maxHealth);
			pokemonName->putxy( text, 15,20);
			//pokemonName->putxy( PokemonNames[ pokemon1->type], 15,20);
		}
	}
};


int main(){

	//SpriteEngine *sprites = new SpriteEngine();
	oam_init( oam_mem, 128);
	//ObjectMemory *objMem = new ObjectMemory();

	REG_DISPCNT= DCNT_MODE0 | DCNT_OBJ | DCNT_OBJ_1D | DCNT_BG0 | DCNT_BG1;

	// object list



	// PLAYER
	Player *player = new Player( Player::Ash);

	// PIKACHU
	Companion *pikachu = new Companion( Companion::Pikachu);

	// BACKGROUND
	//int backgroundID = 0;
	int charblockIndex = 0;
	int screenblockIndex = 16;
	memcpy32(pal_bg_bank[0], litaCityPal, litaCityPalLen/4);
	memcpy32(tile_mem[charblockIndex], litaCityTiles, litaCityTilesLen/4);
	Tilemap *tilemap = new Tilemap( litaCityMap, 3, 2, 0, screenblockIndex);
	tilemap->update( 0,0);
	REG_BG0CNT = BG_CBB(charblockIndex) | BG_SBB(screenblockIndex) | BG_4BPP | BG_REG_64x64 | BG_PRIO(0);


	//objMem->add( player);
	//objMem->add( pikachu);
	player->show();
	pikachu->show();
	//oam_copy( oam_mem, obj_buffer, objCount);

	// initialization
	player->x = 16*16;
	player->y = 30*16;


	enum {OVERWORLD, BATTLE};
	int mode = OVERWORLD;

	Pokemon *pokemon = new Pokemon( PokemonType::Pikachu, 1);

	Battle *battle = new Battle( player, 0);
	//objMem->add( battle);
	battle->show();

	// BATTLE BACKGROUND
/*
	memcpy32(pal_bg_bank[0], ground1Pal, ground1PalLen/4);
	memcpy32(tile_mem[0], ground1Tiles, ground1TilesLen/4);
	memcpy16( se_mem[4], ground1Map, ground1MapLen);
	REG_BG0CNT = BG_CBB(0) | BG_SBB(4) | BG_4BPP | BG_REG_32x32 | BG_PRIO(0);
	REG_BG0HOFS = 0;
	REG_BG0VOFS = 0;

	// MENU

	memcpy32(pal_bg_bank[1], dialog1Pal, dialog1PalLen/4);
	memcpy32(tile_mem[1], dialog1Tiles, dialog1TilesLen/4);
	memcpy16( se_mem[12], dialog1Map, dialog1MapLen);
	REG_BG1CNT = BG_CBB(1) | BG_SBB(12) | BG_4BPP | BG_REG_32x32 | BG_PRIO(0);
	REG_BG1HOFS = 0;
	REG_BG1VOFS = -112;
*/

	// TEXT AND CURSOR

	unsigned short textPalette[16];
	textPalette[0] = RGB15_SAFE(0,0,0);
	textPalette[1] = RGB15_SAFE(31,15,15);
	textPalette[2] = RGB15_SAFE(1,1,31);

	int tileID          = SpriteEngine::instance()->addTiles( fontTiles, fontTilesLen);
	int textPaletteBank = SpriteEngine::instance()->addPalette( &textPalette[0], 32);
	Text *text1 = new Text( tileID, textPaletteBank);
	Text *text2 = new Text( tileID, textPaletteBank);
	Text *text3 = new Text( tileID, textPaletteBank);
	Text *text4 = new Text( tileID, textPaletteBank);
	text1->putxy( PowerNames[ pokemon->powers[0]], 15,120);
	text2->putxy( PowerNames[ pokemon->powers[1]], 15,128);
	text3->putxy( PowerNames[ pokemon->powers[2]], 15,136);
	text4->putxy( PowerNames[ pokemon->powers[3]], 15,144);
	Text *cursor = new Text( tileID, textPaletteBank);
	cursor->putxy( ">", 10, 120);
	cursor->sprites()[0].attr0 = ATTR0_SQUARE | ATTR0_AFF;
	cursor->sprites()[0].attr1 = ATTR1_SIZE_8x8 | ATTR1_AFF_ID(0);
	OBJ_AFFINE *cursorAff = &ObjectMemory::instance()->obj_aff_buffer[0];
	obj_aff_identity(cursorAff);
	int cursorPos = 0;

	/*objMem->add( text1);
	objMem->add( text2);
	objMem->add( text3);
	objMem->add( text4);
	objMem->add( cursor);*/
	text1->show();
	text2->show();
	text3->show();
	text4->show();
	cursor->show();


	int loopCounter = 0;

	while(1)
	{
		vid_vsync();
		key_poll();


		switch( mode){
		case OVERWORLD:
			player->update();
			pikachu->update( player->x, player->y);
			tilemap->update( player->x, player->y);

			player->updateScreen( tilemap->x, tilemap->y);
			pikachu->updateScreen( tilemap->x, tilemap->y);
			player->setRepaint(true);
			pikachu->setRepaint(true);

			//if( loopCounter > 1000)
			if( key_hit( KEY_A)){
				player->hide();
				pikachu->hide();
				battle->setup();
				mode = BATTLE;
			}
			break;

		case BATTLE:
			if( key_hit( KEY_DOWN)){
				cursorPos = (cursorPos + 1) % 4;
			}
			if( key_hit( KEY_UP)){
				cursorPos = (cursorPos + 3) % 4;
			}
			cursor->moveto( 10, 120+cursorPos*8);
			cursor->setRepaint( true);
			obj_aff_scale_inv(cursorAff, 1<<8, (1<<8)-100);
			obj_aff_copy(obj_aff_mem, cursorAff, 1);

			battle->update();

			break;
		}

		int count = 0;
		//Object *objList = Object[5];
		ObjectMemory::instance()->update();

		loopCounter ++;
	}

	return 0;
}

