
//{{BLOCK(litaCity)

//======================================================================
//
//	litaCity, 768x512@4, 
//	+ palette 16 entries, not compressed
//	+ 152 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 96x64 
//	Total size: 32 + 4864 + 12288 = 17184
//
//	Time-stamp: 2020-03-27, 22:52:40
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_LITACITY_H
#define GRIT_LITACITY_H

#define litaCityTilesLen 4864
extern const unsigned int litaCityTiles[1216];

#define litaCityMapLen 12288
extern const unsigned short litaCityMap[6144];

#define litaCityPalLen 32
extern const unsigned short litaCityPal[16];

#endif // GRIT_LITACITY_H

//}}BLOCK(litaCity)
