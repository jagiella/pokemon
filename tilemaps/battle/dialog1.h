
//{{BLOCK(dialog1)

//======================================================================
//
//	dialog1, 256x256@4, 
//	+ palette 16 entries, not compressed
//	+ 5 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x32 
//	Total size: 32 + 160 + 2048 = 2240
//
//	Time-stamp: 2020-03-28, 23:49:35
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_DIALOG1_H
#define GRIT_DIALOG1_H

#define dialog1TilesLen 160
extern const unsigned int dialog1Tiles[40];

#define dialog1MapLen 2048
extern const unsigned short dialog1Map[1024];

#define dialog1PalLen 32
extern const unsigned short dialog1Pal[16];

#endif // GRIT_DIALOG1_H

//}}BLOCK(dialog1)
