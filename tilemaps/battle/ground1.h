
//{{BLOCK(ground1)

//======================================================================
//
//	ground1, 256x256@4, 
//	+ palette 16 entries, not compressed
//	+ 55 tiles (t|f reduced) not compressed
//	+ regular map (in SBBs), not compressed, 32x32 
//	Total size: 32 + 1760 + 2048 = 3840
//
//	Time-stamp: 2020-03-28, 23:36:53
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_GROUND1_H
#define GRIT_GROUND1_H

#define ground1TilesLen 1760
extern const unsigned int ground1Tiles[440];

#define ground1MapLen 2048
extern const unsigned short ground1Map[1024];

#define ground1PalLen 32
extern const unsigned short ground1Pal[16];

#endif // GRIT_GROUND1_H

//}}BLOCK(ground1)
