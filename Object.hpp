/*
 * Object.hpp
 *
 *  Created on: 21 Apr 2020
 *      Author: jagiella
 */

#ifndef OBJECT_HPP_
#define OBJECT_HPP_


extern "C" {
#include "tonc_oam.h"
//#include "tonc_video.h"
//#include "tonc_input.h"
//#include "tonc_bios.h"
//#include "tonc_nocash.h"
}

#include <cstdlib>
#include <list>
using namespace std;

class Object{
protected:
	// sprite handling
	//static SpriteEngine s_spriteEngine;
	bool __repaint = false;
	int __numSprites;
	OBJ_ATTR* __sprites = nullptr;
public:
	Object(){
		__numSprites = 0;
		//sprites = nullptr;
	};
	void show();
	void hide(){
//		for( int i=0; i<__numSprites; i++)
		obj_hide_multi( __sprites, __numSprites);
	};
	void allocate( int numSprites){
		this->__numSprites = numSprites;
		//__sprites = (OBJ_ATTR*) malloc( numSprites * sizeof(OBJ_ATTR));
		__sprites = (OBJ_ATTR*) realloc( (void*)__sprites, numSprites * sizeof(OBJ_ATTR));

	};
	int count(){
		return __numSprites;
	};
	OBJ_ATTR* sprites(){
		return __sprites;
	}
	bool repaint(){
		return __repaint;
	}
	void setRepaint( bool value){
		__repaint = value;
	}

};


class ObjectMemory{
private:
	static ObjectMemory *s_instance;
public:
	static ObjectMemory *instance(){
		if( s_instance == nullptr)
			s_instance = new ObjectMemory();
		return s_instance;
	}

private:
	OBJ_ATTR obj_buffer[128];
	list<Object*> objList;
	int prevObjCount;
	ObjectMemory(){
		prevObjCount = 0;
	}

public:
	OBJ_AFFINE *obj_aff_buffer = (OBJ_AFFINE*)obj_buffer;
	void add( Object* obj){
		objList.push_back( obj);
	}

	void update(){
		int count = 0;
		for( auto *obj: objList){
			//obj->setRepaint( true);
			if( obj->repaint()){
				oam_copy( &oam_mem[count], obj->sprites(), obj->count());
				obj->setRepaint( false);
			}
			count+=obj->count();
		}

		//for( int i=count; i<prevObjCount)
		if( prevObjCount>count)
			obj_hide_multi( &oam_mem[count], prevObjCount-count);
		prevObjCount = count;
	}
};


#endif /* OBJECT_HPP_ */
