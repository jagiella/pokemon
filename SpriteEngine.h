/*
 * SpriteEngine.h
 *
 *  Created on: 20.10.2019
 *      Author: nick
 */

#ifndef SPRITEENGINE_H_
#define SPRITEENGINE_H_



class SpriteEngine{
private:
	const int BYTES_PER_TILE4 = 8*8/2; // 8x8 halfbytes
	const int INTS_PER_TILE4 = 8*8/8;

	int palBankCount;
	int tilesCount;
	static SpriteEngine *s_instance;

	SpriteEngine();
public:
	static SpriteEngine *instance();

	unsigned int addTiles( const unsigned int *tiles, int tilesCount);
	unsigned int allocateTiles( int tilesCount);
	void copyTiles( unsigned int tidSrc, unsigned int tidDst, const unsigned int *tiles, int tilesCount);
	unsigned int addPalette( const unsigned short *pal, int palLen);
	void copyPalette( unsigned int pbDst, const unsigned short *palette, int paletteLen);
};

class SpriteObject{
	// STATIC
//protected:
//	static signed int sTid;
//	static signed int sPb;
//public:
//	virtual static void init( SpriteEngine *spriteEngine) = 0;
};



#endif /* SPRITEENGINE_H_ */
