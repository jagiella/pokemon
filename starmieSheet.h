
//{{BLOCK(starmieSheet)

//======================================================================
//
//	starmieSheet, 256x32@4, 
//	+ palette 16 entries, not compressed
//	+ 128 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 4096 = 4128
//
//	Time-stamp: 2020-03-24, 23:39:06
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_STARMIESHEET_H
#define GRIT_STARMIESHEET_H

#define starmieSheetTilesLen 4096
extern const unsigned int starmieSheetTiles[1024];

#define starmieSheetPalLen 32
extern const unsigned short starmieSheetPal[16];

#endif // GRIT_STARMIESHEET_H

//}}BLOCK(starmieSheet)
