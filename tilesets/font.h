
//{{BLOCK(font)

//======================================================================
//
//	font, 128x64@4, 
//	+ palette 16 entries, not compressed
//	+ 128 tiles not compressed
//	Total size: 32 + 4096 = 4128
//
//	Time-stamp: 2020-04-02, 12:56:16
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_FONT_H
#define GRIT_FONT_H

#define fontTilesLen 4096
extern const unsigned int fontTiles[1024];

#define fontPalLen 32
extern const unsigned short fontPal[16];

#endif // GRIT_FONT_H

//}}BLOCK(font)
