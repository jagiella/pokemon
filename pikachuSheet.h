
//{{BLOCK(pikachuSheet)

//======================================================================
//
//	pikachuSheet, 512x32@4, 
//	+ palette 16 entries, not compressed
//	+ 256 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 8192 = 8224
//
//	Time-stamp: 2020-03-28, 15:25:37
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PIKACHUSHEET_H
#define GRIT_PIKACHUSHEET_H

#define pikachuSheetTilesLen 8192
extern const unsigned int pikachuSheetTiles[2048];

#define pikachuSheetPalLen 32
extern const unsigned short pikachuSheetPal[16];

#endif // GRIT_PIKACHUSHEET_H

//}}BLOCK(pikachuSheet)
