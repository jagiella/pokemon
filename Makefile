#
# A more complicated makefile
#

UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
DEVKITARM := /home/nick/Downloads/devkitARM
TONCLIB   := /home/nick/Code/code/tonclib
VBA       := vba -4 
endif
ifeq ($(UNAME), Darwin)
DEVKITARM := /Users/jagiella/Downloads/devkitARM
TONCLIB   := /Users/jagiella/Downloads/code/tonclib
VBA       := /Applications/visualboyadvance-m.app/Contents/MacOS/visualboyadvance-m --verbose 
endif

PATH := $(DEVKITARM)/bin:$(PATH)

# --- Project details -------------------------------------------------

PROJ    := pokemon
TARGET  := $(PROJ)

# C++
OBJCPP  := pokemon.o SpriteEngine.o Tilemap.o Object.o
# C
OBJC    := ashSheet.o mistySheet.o pikachuSheet.o starmieSheet.o
OBJC    += tilemaps/litaCity.o
OBJC    += tilemaps/battle/dialog1.o tilemaps/battle/ground1.o
OBJC    += tilesets/font.o
OBJC    += sprites/pokemon/f19.o
OBJC    += sprites/pokemon/f25.o
OBJC    += sprites/pokemon/b19.o
OBJC    += sprites/pokemon/b25.o
OBJC    += $(TONCLIB)/src/tonc_obj_affine.o $(TONCLIB)/src/tonc_oam.o $(TONCLIB)/src/tonc_input.o $(TONCLIB)/src/tonc_core.o
# ASM
OBJAS   := $(TONCLIB)/asm/tonc_memcpy.o $(TONCLIB)/asm/tonc_bios.o $(TONCLIB)/asm/tonc_nocash.o $(TONCLIB)/asm/sin_lut.o 

# --- Build defines ---------------------------------------------------

PREFIX  := $(DEVKITARM)/bin/arm-none-eabi-
CPP     := $(PREFIX)g++
CC      := $(PREFIX)gcc
LD      := $(PREFIX)g++
OBJCOPY := $(PREFIX)objcopy
AS      := $(PREFIX)as

ARCH    := -mthumb-interwork -mthumb
SPECS   := -specs=gba.specs

CFLAGS  := $(ARCH) -O5 -Wall -fno-strict-aliasing -I$(TONCLIB)/include -Wno-narrowing
LDFLAGS := $(ARCH) $(SPECS)
ASFLAGS  := $(ARCH) -I$(TONCLIB)/include

.PHONY : build clean

# --- Build -----------------------------------------------------------
# Build process starts here!
all: $(TARGET).gba
	$(VBA) $(TARGET).gba 

# Strip and fix header (step 3,4)
$(TARGET).gba : $(TARGET).elf
	$(OBJCOPY) -v -O binary $< $@
	#-@gbafix $@

# Link (step 2)
$(TARGET).elf : $(OBJC) $(OBJCPP) $(OBJAS)
	$(CPP) -x assembler-with-cpp -mthumb-interwork -mthumb -c gba_crt0.s -o gba_crt0.o
	$(LD) $^ $(LDFLAGS) -o $@

# Compile (step 1)
$(OBJCPP) : %.o : %.cpp
	$(CPP) -c $< $(CFLAGS) -o $@

$(OBJC) : %.o : %.c
	$(CC) -c $< $(CFLAGS) -o $@

$(OBJAS) : %.o : %.s
	#$(AS) -c $< $(AFLAGS) -o $@
	$(CC) -MMD -MP -MF $(DEPSDIR)/$*.d -x assembler-with-cpp $(ASFLAGS) -c $< -o $@
	
		
# --- Clean -----------------------------------------------------------

clean : 
	@rm -fv gba_crt0.o
	@rm -fv *.gba
	@rm -fv *.elf
	@rm -fv $(OBJCPP) $(OBJC) $(OBJAS)

#EOF