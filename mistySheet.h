
//{{BLOCK(mistySheet)

//======================================================================
//
//	mistySheet, 256x32@4, 
//	+ palette 16 entries, not compressed
//	+ 128 tiles Metatiled by 4x4 not compressed
//	Total size: 32 + 4096 = 4128
//
//	Time-stamp: 2020-03-24, 22:39:14
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.12
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_MISTYSHEET_H
#define GRIT_MISTYSHEET_H

#define mistySheetTilesLen 4096
extern const unsigned int mistySheetTiles[1024];

#define mistySheetPalLen 32
extern const unsigned short mistySheetPal[16];

#endif // GRIT_MISTYSHEET_H

//}}BLOCK(mistySheet)
